//
//  ViewController.swift
//  T1E01RubenGarcia
//
//  Created by Ruben Garcia on 2/19/17.
//  Copyright © 2017 Ruben Garcia. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var myArray = [1,2,3,4,5]

    override func viewDidLoad() {
        super.viewDidLoad()
        print("Rubén Darío García Astudillo")
        print()
        
        print("First array: \(myArray)")
        var totalSum = sumArray(customArray: myArray)
        print("The result of the first sum is: \(totalSum)")
        print()
        
        let secondArray = randomNumbersArray()
        print("Second array: \(secondArray)")
        totalSum = sumArray(customArray: secondArray)
        print("The result of the second sum is: \(totalSum)")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //This method returns the sum of the values of an array
    func sumArray(customArray: [Int]) -> Int {
        var total = 0
        for myNumber in customArray {
            total = myNumber + total
        }
        return total
    }

    //This method returns an array with random values
    func randomNumbersArray() -> [Int] {
        var randomArray = [Int]()
        for _ in 0...9 {
            let myRandomNumber = Int(arc4random_uniform(10))
            randomArray.append(myRandomNumber)
        }
        return randomArray
    }
    
}

