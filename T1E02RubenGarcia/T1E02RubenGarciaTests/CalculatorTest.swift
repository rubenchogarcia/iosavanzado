//
//  CalculatorTest.swift
//  T1E02RubenGarcia
//
//  Created by Ruben Garcia on 2/19/17.
//  Copyright © 2017 Ruben Garcia. All rights reserved.
//

import XCTest
@testable import T1E02RubenGarcia

class CalculatorTest: XCTestCase {
    
    var calculator = Calculator()
    
    override func setUp() {
        super.setUp()
        //self.calculator = Calculator()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
//    func testExample() {
//        // This is an example of a functional test case.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//    }
//    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

    
    func testAdd(){
        self.calculator.operator1 = 5
        self.calculator.operator2 = 5
        self.calculator.operation = "+"
        XCTAssertEqual(self.calculator.makeOperation(),10,"Error")
    }
    
    func testSubstract(){
        self.calculator.operator1 = 5
        self.calculator.operator2 = 5
        self.calculator.operation = "-"
        XCTAssertEqual(self.calculator.makeOperation(),0,"Error")
    }

    func testMultiply(){
        self.calculator.operator1 = 5
        self.calculator.operator2 = 5
        self.calculator.operation = "*"
        XCTAssertEqual(self.calculator.makeOperation(),25,"Error")
    }
    
    func testDivide(){
        self.calculator.operator1 = 5
        self.calculator.operator2 = 5
        self.calculator.operation = "/"
        XCTAssertEqual(self.calculator.makeOperation(),1,"Error")
    }
    
    func testDivideCero(){
        self.calculator.operator1 = 5
        self.calculator.operator2 = 0
        self.calculator.operation = "/"
        XCTAssertEqual(self.calculator.makeOperation(),0.0,"Error")
    }
    

    
    

}
